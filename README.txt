## Run Updates to core
composer update "drupal/core-*" --with-all-dependencies


## Run on Coruscant
fin drush cex --destination=./profiles/contrib/coruscant/config/install --yes
rsync -r config/sync/ ../coruscant_distro/config/install/ --delete

## Run these after exporting the configs
find config/install/ -type f -exec sed -i '' '/^uuid: /d' {} \;
find config/install/ -type f -exec sed -i '' '/_core:/{N;d;}' {} \;

## Working on Theme

# make changes in distro, then rsync to site
rsync -r web/profiles/contrib/coruscant/themes/contrib/vibrant/ ../coruscant_distro/themes/contrib/vibrant/
